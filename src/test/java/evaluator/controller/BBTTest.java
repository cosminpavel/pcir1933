package evaluator.controller;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.repository.IntrebariRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BBTTest {
    public AppController appController;
    public IntrebariRepository intrebariRepository;

    @Before
    public void setUp() throws Exception {
        intrebariRepository = new IntrebariRepository(null);
        appController = new AppController(intrebariRepository);
    }
    @Test
    public void addNewIntrebareValida() {
        int size = intrebariRepository.getIntrebari().size();
        try {
            appController.addNewIntrebare("Cat face 1+1?","1)2","2)3","3)4","3","Matematica");
        } catch (DuplicateIntrebareException e) {
            assertTrue(false);
        } catch (InputValidationFailedException e) {
           assertTrue(false);
        }
        assertTrue((size+1) == intrebariRepository.getIntrebari().size());
    }
    @Test
    public void addNewIntrebareVar1Vida() {
        int size = intrebariRepository.getIntrebari().size();
        try {
            appController.addNewIntrebare("Cat face 1+1?","","2)3","3)4","3","Matematica");
        } catch (DuplicateIntrebareException e) {
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().equals("Varianta1 este vida!"));
        }
        assertTrue((size) == intrebariRepository.getIntrebari().size());
    }
    @Test
    public void addNewIntrebareVar2Vida() {
        int size = intrebariRepository.getIntrebari().size();
        try {
            appController.addNewIntrebare("Cat face 1+1?","1)2","","3)4","3","Matematica");
        } catch (DuplicateIntrebareException e) {
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().equals("Varianta2 este vida!"));
        }
        assertTrue((size) == intrebariRepository.getIntrebari().size());
    }
    @Test
    public void addNewIntrebareDuplicata() {
        int size = intrebariRepository.getIntrebari().size();
        try {
            appController.addNewIntrebare("Cat face 1+1?","1)2","2)3","3)4","3","Matematica");
            assertTrue((size+1) == intrebariRepository.getIntrebari().size());
            appController.addNewIntrebare("Cat face 1+1?","1)2","2)3","3)4","3","Matematica");
        } catch (DuplicateIntrebareException e) {
            assertTrue(true);
        } catch (InputValidationFailedException e) {
            assertTrue(false);
        }
        assertTrue((size+1) == intrebariRepository.getIntrebari().size());
    }
    @Test
    public void addNewIntrebareVar3Vida() {
        int size = intrebariRepository.getIntrebari().size();
        try {
            appController.addNewIntrebare("Cat face 1+1?","1)2","2)3","","3","Matematica");
        } catch (DuplicateIntrebareException e) {
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            assertTrue(e.getMessage().equals("Varianta3 este vida!"));
        }
        assertTrue((size) == intrebariRepository.getIntrebari().size());
    }
    @Test
    public void addNewIntrebareFaraSemnIntrebarii() {
        int size = intrebariRepository.getIntrebari().size();
        try {
            appController.addNewIntrebare("Cat face 1+1","1)2","2)3","3)4","3","Matematica");
        } catch (DuplicateIntrebareException e) {
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            assertTrue(true);
        }
        assertTrue((size) == intrebariRepository.getIntrebari().size());
    }
    @Test
    public void addNewIntrebareFaraMajuscula() {
        int size = intrebariRepository.getIntrebari().size();
        try {
            appController.addNewIntrebare("cat face 1+1?","1)2","2)3","3)4","3","Matematica");
        } catch (DuplicateIntrebareException e) {
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            assertTrue(true);
        }
        assertTrue((size) == intrebariRepository.getIntrebari().size());
    }
    @Test
    public void addNewIntrebarevida() {
        int size = intrebariRepository.getIntrebari().size();
        try {
            appController.addNewIntrebare("","1)2","2)3","3)4","3","Matematica");
        } catch (DuplicateIntrebareException e) {
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            assertTrue(true);
        }
        assertTrue((size) == intrebariRepository.getIntrebari().size());
    }
    @Test
    public void addNewIntrebare() {
        int size = intrebariRepository.getIntrebari().size();
        try {
            appController.addNewIntrebare("D?","1)da","2)nu","3)Da","3","Abstract");
        } catch (DuplicateIntrebareException e) {
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            assertTrue(false);
        }
        assertTrue((size + 1) == intrebariRepository.getIntrebari().size());
    }
    @Test
    public void addNewIntrebareVar1Incorecta() {
        int size = intrebariRepository.getIntrebari().size();
        try {
            appController.addNewIntrebare("Cat face 1+1?","11","2)3","3)4","3","Matematica");
        } catch (DuplicateIntrebareException e) {
            assertTrue(false);
        } catch (InputValidationFailedException e) {
            assertTrue(true);
        }
        assertTrue((size) == intrebariRepository.getIntrebari().size());
    }
    @After
    public void tearDown() throws Exception {
    }
}