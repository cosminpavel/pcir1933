package evaluator.controller;

import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class WBTTest {
    IntrebariRepository intrebariRepository;
    AppController appController;
    @Before
    public void setup(){
        intrebariRepository = new IntrebariRepository(null);
        appController = new AppController(intrebariRepository);
    }

    @Test
    public void createTestValid() {
        List<Intrebare> intrebari = new ArrayList<>();
        try {
            intrebari.add( new Intrebare("Ce?","1)a","2)b","3)c","3","Stiinta"));
            intrebari.add(new Intrebare("De ce?","1)a","2)b","3)c","3","Politica"));
            intrebari.add(new Intrebare("Cat e 2+2?","1)4","2)b","3)c","3","Matematica"));
            intrebari.add(new Intrebare("Cat e 2+2?","1)4","2)b","3)c","3","Cultura"));
            intrebari.add(new Intrebare("Cat e 2+2?","1)4","2)b","3)c","3","Geografie"));
            intrebari.add(new Intrebare("Cat e 2+2?","1)4","2)b","3)c","3","Matematica"));

        } catch (InputValidationFailedException e) {
           assertTrue(false);
        }

        intrebariRepository.setIntrebari(intrebari);
        evaluator.model.Test test = null;
        try {
            test = appController.createNewTest();
        } catch (NotAbleToCreateTestException e) {
           assertTrue(false);
        }
        assertTrue(test.getIntrebari().size() == 5);
    }
    @Test
    public void createTestPutineIntrebari() {
        List<Intrebare> intrebari = new ArrayList<>();
        try {
            intrebari.add( new Intrebare("Ce ?","1)a","2)b","3)c","3","Stiinta"));
            intrebari.add(new Intrebare("De ce?","1)a","2)b","3)c","3","Politica"));

        } catch (InputValidationFailedException e) {
            assertTrue(false);
        }

        intrebariRepository.setIntrebari(intrebari);
        evaluator.model.Test test = null;
        try {
            test = appController.createNewTest();
        } catch (NotAbleToCreateTestException e) {
            if (e.getMessage().equals("Nu exista suficiente intrebari pentru crearea unui test!(5)"))
                assertTrue(true);
        }
        if(test != null){
            assertTrue(false);
        }
    }
    @Test
    public void createTestPutineDomenii() {
        List<Intrebare> intrebari = new ArrayList<>();
        try {
            intrebari.add( new Intrebare("Ce?","1)a","2)b","3)c","3","Stiinta"));
            intrebari.add(new Intrebare("De ce?","1)a","2)b","3)c","3","Politica"));
            intrebari.add(new Intrebare("Cat e 2+2?","1)4","2)b","3)c","3","Matematica"));
            intrebari.add(new Intrebare("Cat e 2+2?","1)4","2)b","3)c","3","Matematica"));
            intrebari.add(new Intrebare("Cat e 2+2?","1)4","2)b","3)c","3","Matematica"));
            intrebari.add(new Intrebare("Cat e 2+2?","1)4","2)b","3)c","3","Matematica"));   } catch (InputValidationFailedException e) {
            assertTrue(false);
        }

        intrebariRepository.setIntrebari(intrebari);
        evaluator.model.Test test = null;
        try {
            test = appController.createNewTest();
        } catch (NotAbleToCreateTestException e) {
            if (e.getMessage().equals("Nu exista suficiente domenii pentru crearea unui test!(5)"))
                assertTrue(true);
        }
        if(test != null){
            assertTrue(false);
        }
    }
}