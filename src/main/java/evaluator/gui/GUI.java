package evaluator.gui;

import evaluator.controller.AppController;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GUI {
    private AppController appController;

    public GUI(AppController appController) {
        this.appController = appController;
    }
    public void start(){
        try {
            getMenu();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
    public void getMenu() throws IOException{
        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
        boolean activ = true;
        String optiune = null;

        while(activ){

            System.out.println("");
            System.out.println("1.Adauga intrebare");
            System.out.println("2.Creeaza test");
            System.out.println("3.Statistica");
            System.out.println("4.Exit");
            System.out.println("");

            optiune = console.readLine();

            switch(optiune){
                case "1" :
                    System.out.println("Scrie intrebarea: ");
                    String intrText = console.readLine();
                    System.out.println("Var1 :");
                    String varianta1 = console.readLine();
                    System.out.println("Var2 :");
                    String varianta2 = console.readLine();
                    System.out.println("Var3 :");
                    String varianta3 = console.readLine();
                    System.out.println("Var corecta :");
                    String variantaCorecta = console.readLine();
                    System.out.println("Domeniu:");
                    String domeniu = console.readLine();
                    try {

                        appController.addNewIntrebare(intrText, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
                    }catch (Exception e){
                        System.err.println(e.getMessage());
                    }
                    break;
                case "2" :
                    try {
                        System.out.println(appController.createNewTest());
                    } catch (NotAbleToCreateTestException e) {
                        System.err.println(e.getMessage());
                    }
                case "3" :
                    Statistica statistica;
                    try {
                        statistica = appController.getStatistica();
                        System.out.println(statistica);
                    } catch (NotAbleToCreateStatisticsException e) {
                        System.out.println("Not able to create!");
                    }

                    break;
                case "4" :
                    appController.saveToFile();
                    activ = false;
                    break;
                default:
                    break;
            }
        }
    }
}
