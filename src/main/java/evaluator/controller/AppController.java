package evaluator.controller;

import java.util.LinkedList;
import java.util.List;

import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.model.Test;
import evaluator.repository.IntrebariRepository;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;

public class AppController {
	
	private IntrebariRepository intrebariRepository;
	public AppController(IntrebariRepository intrebariRepository) {
		this.intrebariRepository = intrebariRepository;
	}
	
	public Intrebare addNewIntrebare(String intrText,String varianta1,String varianta2, String varianta3, String variantaCorecta,String domeniu) throws DuplicateIntrebareException, InputValidationFailedException {
		Intrebare intrebare = new Intrebare(intrText, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
		intrebariRepository.addIntrebare(intrebare);
		
		return intrebare;
	}

	public Test createNewTest() throws NotAbleToCreateTestException{
		
		if(intrebariRepository.getIntrebari().size() <= 4) {
            throw new NotAbleToCreateTestException("Nu exista suficiente intrebari pentru crearea unui test!(5)");
        }
		if(intrebariRepository.getNumberOfDistinctDomains() <= 4) {
            throw new NotAbleToCreateTestException("Nu exista suficiente domenii pentru crearea unui test!(5)");
        }
		List<Intrebare> testIntrebari = new LinkedList<Intrebare>();
		List<String> domenii = new LinkedList<String>();
		Intrebare intrebare;
		Test test = new Test();
		
		while(testIntrebari.size() != 5){
			intrebare = intrebariRepository.pickRandomIntrebare();
			if(!testIntrebari.contains(intrebare) && !domenii.contains(intrebare.getDomeniu())){
				testIntrebari.add(intrebare);
				domenii.add(intrebare.getDomeniu());
			}
		}
		
		test.setIntrebari(testIntrebari);
		return test;
		
	}
	
	public Statistica getStatistica() throws NotAbleToCreateStatisticsException {

        if (intrebariRepository.getIntrebari().isEmpty()){
            System.err.println("Repo gol" + intrebariRepository.getIntrebari());
            throw new NotAbleToCreateStatisticsException("Repository-ul nu contine nicio intrebare!");
        }
		Statistica statistica = new Statistica();
		for(String domeniu : intrebariRepository.getDistinctDomains()){
			statistica.add(domeniu, intrebariRepository. getNumberOfIntrebariByDomain(domeniu));
		}
		
		return statistica;
	}
	public void saveToFile(){
	    intrebariRepository.saveToFile();
    }


}
