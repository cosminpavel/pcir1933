package evaluator.repository;

import java.io.*;
import java.util.*;


import evaluator.model.Intrebare;
import evaluator.exception.DuplicateIntrebareException;

public class IntrebariRepository {
	
	private List<Intrebare> intrebari;
	private String file;
	public IntrebariRepository(String file) {
		this.file = file;
		setIntrebari(new LinkedList<Intrebare>());
		if(file != null) {
			loadIntrebariFromFile(file);
		}
	}
	
	public void addIntrebare(Intrebare i) throws DuplicateIntrebareException{
		if(exists(i))
			throw new DuplicateIntrebareException("Intrebarea deja exista!");
		intrebari.add(i);
	}
	
	public boolean exists(Intrebare i){
		for(Intrebare intrebare : intrebari)
			if(intrebare.equals(i))
				return true;
		return false;
	}
	
	public Intrebare pickRandomIntrebare(){
		Random random = new Random();
		Integer ra = random.nextInt(intrebari.size());
        //System.out.println(ra);
		return intrebari.get(ra);
	}
	
	public int getNumberOfDistinctDomains(){
		return getDistinctDomains().size();
		
	}
	
	public Set<String> getDistinctDomains(){
		Set<String> domains = new TreeSet<String>();
		for(Intrebare intrebre : intrebari)
			domains.add(intrebre.getDomeniu());
		return domains;
	}
	
	public List<Intrebare> getIntrebariByDomain(String domain){
		List<Intrebare> intrebariByDomain = new LinkedList<Intrebare>();
		for(Intrebare intrebare : intrebari){
			if(intrebare.getDomeniu().equals(domain)){
				intrebariByDomain.add(intrebare);
			}
		}
		
		return intrebariByDomain;
	}
	
	public int getNumberOfIntrebariByDomain(String domain){
		return getIntrebariByDomain(domain).size();
	}
	
	public void loadIntrebariFromFile(String f){

		BufferedReader br = null; 
		String line = null;
		List<String> intrebareAux;
		Intrebare intrebare;
		
		try{
			br = new BufferedReader(new FileReader(f));
			line = br.readLine();
			while(line != null){
				intrebareAux = new LinkedList<String>();
				while(!line.equals("##")){
					intrebareAux.add(line);
					line = br.readLine();
				}
				intrebare = new Intrebare();
				intrebare.setEnunt(intrebareAux.get(0));
				intrebare.setVarianta1(intrebareAux.get(1));
				intrebare.setVarianta2(intrebareAux.get(2));
				intrebare.setVarianta3(intrebareAux.get(3));
				intrebare.setVariantaCorecta(intrebareAux.get(4));
				intrebare.setDomeniu(intrebareAux.get(5));
				intrebari.add(intrebare);
				line = br.readLine();
			}
			//System.out.println(intrebari);
		}
		catch (IOException e) {
			System.err.println(e.getMessage());
		}
		finally{
			try {
				br.close();
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}
	}
	
	public List<Intrebare> getIntrebari() {
		return intrebari;
	}

	public void setIntrebari(List<Intrebare> intrebari) {
		this.intrebari = intrebari;
	}
    public void saveToFile(){
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {


            fw = new FileWriter(file);
            bw = new BufferedWriter(fw);
            for (Intrebare intrebare:intrebari){
                bw.write(intrebare.getEnunt() +"\n");
                bw.write(intrebare.getVarianta1() +"\n");
                bw.write(intrebare.getVarianta2() +"\n");
                bw.write(intrebare.getVarianta3() +"\n");
                bw.write(intrebare.getVariantaCorecta() +"\n");
                bw.write(intrebare.getDomeniu() +"\n");
                bw.write("##\n");
            }

           // System.out.println("Done");

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (bw != null)
                    bw.close();

                if (fw != null)
                    fw.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }
    }
}
